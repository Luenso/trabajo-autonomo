package tareamovil1.luissolorzano.facci.tareamovil1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button convertirFahren, convertirCenti;
    EditText valorFa, valorCenti;
    TextView resultadoFa, resultadoCenti;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        convertirFahren = findViewById(R.id.buttonFahren);
        convertirCenti = findViewById(R.id.buttonCenti);
        valorFa = findViewById(R.id.editTextValorFa);
        resultadoFa= findViewById(R.id.textViewResulFa);
        valorCenti = findViewById(R.id.editTextValorCenti);
        resultadoCenti= findViewById(R.id.textViewResulCenti);



        Log.e("OnCreate", "Luis Enrique Solorzano Medina");

        convertirCenti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                convertidorCenti(v);
            }
        });

        convertirFahren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                convertidorFa(v);
            }
        });

    }
    public void convertidorCenti(View view){
        String valorCadena = valorCenti.getText().toString();
        if (valorCadena.equals("")) {
            Toast.makeText(getApplicationContext(), "Ingrese el valor", Toast.LENGTH_SHORT).show();

        } else {
            //int valor = Integer.parseInt(valorCadena);
            double VALOR = Double.parseDouble(valorCadena);
            double resul;
            resul = (VALOR - 32)/1.8;
            String resulCadena = String.valueOf(resul);
            resultadoCenti.setText(resulCadena);
        }
    }
    public void convertidorFa(View view){
        String valorcadena = valorFa.getText().toString();
        if (valorcadena.equals("")) {
            Toast.makeText(getApplicationContext(), "Ingrese el valor", Toast.LENGTH_SHORT).show();

        } else {
            //int valor = Integer.parseInt(valorcadena);
            double VALOR = Double.parseDouble(valorcadena);
            double result;
            result = (VALOR * 1.8)+32;
            String resulCadena = String.valueOf(result);
            resultadoFa.setText(resulCadena);
        }
    }
}
